class SecurityCtrl extends Monocle.Controller

    context: "signup"

    elements:
        ".form" : "form"
        "[data-role=signup]" : "signup"
        "[data-role=login]" : "login"

    events:
        "tap [data-context=login]"  : "viewLogin"
        "tap [data-context=signup]" : "viewSignup"
        "tap [data-action=login]"  : "onAction"
        "tap [data-action=signup]" : "onAction"        


    initialize: ->
        console.log "initialize"
        do @form.show
        do @viewSignup

    onAction: =>
        console.log "onAction"

    viewSignup: =>
        console.log "viewSignup"
        @context = "signup"
        @signup.show()
        @login.hide()

    viewLogin: =>
        console.log "viewLogin"
        @context = "login"
        @login.show()
        @signup.hide()

Lungo.ready ->
    __Controller.Security = new SecurityCtrl "section#login"
    do __Controller.Security.initialize